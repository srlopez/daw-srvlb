FROM node
#  docker build -f lb.Dockerfile -t s2:1.0 .
#  docker run -d --rm -p 5000:5000 s2:1.0
WORKDIR /app
# ====
ARG CACHEBUST=1
RUN git clone https://gitlab.com/srlopez/daw-srv02.git .
# ====
RUN npm install
ENV hello=Docker!!!
ENTRYPOINT [ "node", "server.js" ]
EXPOSE 5000